import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';

interface contactoForm {
    name : String,
    lastname: String,
    birthdate: Date,
    pay: Number
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
    contacto : any = {
        name : '12',
        lastname: '',
        birthdate: new Date(),
        pay: 0
    }
    submitted = false;
	
    constructor(private formBuilder: FormBuilder) { }
 
    ngOnInit() {
        this.contacto = this.formBuilder.group({
            name: ['', Validators.compose([
                Validators.required,
            ])],            
            lastname: ['', , Validators.compose([
                Validators.required,
            ])],             
            birthdate: ['', Validators.required],            
            pay: ['', Validators.compose([
                Validators.required,
            ])],                  
        });
    }
 
    get f() { return this.contacto.controls; }
 
    onSubmit() {
        this.submitted = true;
 
        if (this.contacto.invalid) {
            return;
        }
 
        alert('Mensaje Enviado !')
    }
    submit() {
        console.log('aqui')
    }
    closmodal() {
    }
}
