import { Component, OnInit, ɵSWITCH_COMPILE_NGMODULE__POST_R3__ } from '@angular/core';
import { UsersService } from "./content.service";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html', 
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  users : any;
  modal = false
  constructor(public usersService: UsersService) { }
  /* constructor(private http: HttpClient){
    this.http.get('https://reqres.in/api/users?page=2');
  } */
  ngOnInit(): void {
    this.getuser()
  }
  getuser() {
    this.usersService.getUsers().subscribe(data => {
      this.users = data.data;
    });
  }
  abrirmodal() {
    this.modal = true
  }
  closemodal() {
    this.modal = false

  }
  detail() {
    this.usersService.getUsers().subscribe(data => {
      this.users = data.data;
    });
  }
  
  
}
